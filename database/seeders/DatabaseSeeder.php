<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Profile;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'name' => 'Ryan',
            'email' => 'ryan.hira@hotmail.nl',
            'role_id' => 3
        ]);
        \App\Models\Profile::factory(10)->create();
        \App\Models\Role::factory(3)->create();




    }
}
