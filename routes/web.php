<?php

use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.layout');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('roles',App\Http\Controllers\RoleController::class);


Route::get('/carbontime',function (){
    $ryan =Carbon::now('Europe/Amsterdam');
    echo $ryan->format('d-m-Y');

});
Route::resource('tabletopgames', \App\Http\Controllers\TabletopGameController::class);
