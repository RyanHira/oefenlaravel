@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h1>Create</h1>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('roles.store') }}"method="post">
                            @csrf
                            <label for="title">Rolnaam</label>
                            <input type="text" name="title" id="title">
                            <button type="submit">Voeg toe</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
