@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h1>Edit</h1>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('roles.update', $role->id) }}"method="post">
                            @csrf
                            @method('patch')
                            <label for="title">Rolnaam</label>
                            <input type="text" name="title" id="title" value="{{ $role->title }}">
                            <button class="btn btn-outline-primary btn-sm" type="submit">edit</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
