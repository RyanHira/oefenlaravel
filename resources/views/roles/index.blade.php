@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h1>Rollen</h1>
                        <a class="btn btn-outline-info btn-sm" href="{{ route('roles.create') }}">Maak rol</a>
                    </div>

                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($roles as $role)
                                <li class="list-group-item">
                                    <a href="{{ route('roles.show', $role->id) }}">{{ $role->title }}</a>
{{--                                    <p>{{ $role->toArray()['updated_at'] }}</p>--}}
{{--                                    <p>{{ $role->updated_at }}</p>--}}
{{--                                    {{ $newYear->locale('nl')->dayName }}--}}

                                    <form class="float-end" style="display:inline-block" action="{{ route('roles.destroy', $role->id) }}" method="post"> @csrf @method('delete')
                                        <button class="btn btn-outline-danger btn-sm m-1" type="submit"><i class="fa-solid fa-trash-can"></i></button>
                                    </form>
                                    <a class="btn btn-outline-info btn-sm float-end m-1" href="{{ route('roles.edit', $role->id) }}"><i class="fa-solid fa-pen-to-square"></i></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
