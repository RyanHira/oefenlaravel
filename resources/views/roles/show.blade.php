@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h1>{{ $role->title }}</h1>
                    </div>

                    <div class="card-body">
                        <ul>
                            @foreach($role->userRole as $user)
                                <li>{{ $user->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
