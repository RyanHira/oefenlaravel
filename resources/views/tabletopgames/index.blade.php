@extends('tabletopgames.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 9 CRUD with Image </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('tabletopgames.create') }}"> Create New TTG</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Image</th>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($tabletopgames as $tabletopgame)
            <tr>
                <td>{{ ++$i }}</td>
                <td><img src="/images/{{ $tabletopgame->image }}" width="100px"></td>
                <td>{{ $tabletopgame->title }}</td>
                <td>
                    <form action="{{ route('tabletopgames.destroy',$tabletopgame->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('tabletopgames.show',$tabletopgame->id) }}">Show</a>

                        <a class="btn btn-primary" href="{{ route('tabletopgames.edit',$tabletopgame->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
                <td>{{ $tabletopgame->updated_at
                             ->format('d/m/Y') }}</td>
            </tr>
        @endforeach
    </table>

    {!! $tabletopgames->links() !!}

@endsection
