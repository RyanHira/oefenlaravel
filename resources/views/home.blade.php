@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('You are logged in!') }}
                            <br>
                        {{ Auth::user()->name }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('games')
    @foreach($games as $game)
        <div class="col-lg-6">
            <a class="portfolio-item" href="#!">
                <div class="caption">
                    <div class="caption-content">
                        <div class="h2">{{ $game->title }}</div>
                        <p class="mb-0">A yellow pencil with envelopes on a clean, blue backdrop!</p>
                    </div>
                </div>
{{--                @dd($game->image)--}}
                <img class="img-fluid" src="/images/{{ $game->image }}" alt="..." />
            </a>
        </div>
    @endforeach
@endsection
