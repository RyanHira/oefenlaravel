<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\TabletopGame;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $games = TabletopGame::paginate(4);
        //$users = User::all();
        $roles = Role::all();
        return view('home',compact('games','roles'));
    }
}
